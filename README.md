Развертывание проекта - [project deploy]

- Клонировать репозиторие ```git clone https://gagik12@bitbucket.org/gagik12/collector.git```

- Перейти в папку проекта ```cd /collector```

- Подтянуть сабмодули ```git submodule update --init --recursive```

- Репозитория модулей могут быть в неактуальном состоянии, нужно выполнить команду, чтоб сабмодули указывали на последний коммит  - ```git submodule update --remote --merge```

- Запустить bash скрипт для сборки docker контейнеров - ```./project-up.sh```

- Добавить domain alias -> ```127.0.0.1 project.docker.local``` в файле хоста локальной машины. Можно выполнить команду ```sudo mcedit /etc/hosts``` для редактирования файла от суперпользователя  

- Зайти в контейнер serverpreparations_phpfpm_1 выполнив скрипт - ```./project-bash.sh```

- В контейнере перейти в директорию cd /var/www/backend

- И подтянуть все зависимости необходимые для запуска php кода на базе фреймворка symfony - ```composer install``` 

- Теперь можно открывать сайт в браузере - project.docker.local